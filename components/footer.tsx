import Link from 'next/link';
import SOAFEELogo from '@/components/soafeelogo';

interface MenuItem {
  name: string;
  url: string;
}

interface FooterProps {
  className?: string;
}

const footerMenu: MenuItem[] = [
  { name: 'Accessibility', url: '/about/accessibility' },
  { name: 'Privacy', url: '/about/privacy' },
  { name: 'Terms of Use', url: '/about/terms_of_use' }
];

const menu = (items: MenuItem[]) => {
  return items.map((item, index) => (
    <Link href={item.url} passHref legacyBehavior key={index}>
      <a className="flex flex-row pb-4 text-soafee-yellow typography-desktop-b2 md:typography-desktop-b3">
        {item.name}
      </a>
    </Link>
  ));
};

const Footer = ({ className }: FooterProps) => {
  return (
    <div className={`${className} pt-[75px] pb-[55px] md:py-[90px] wrapper-lg bg-soafee-black font-aspekta`}>
      <div className="flex flex-col md:flex-row">
        <div className="md:basis-2/4 mb-[75px] md:mb-0 fill-white">
          <SOAFEELogo className="w-[185px] lg:w-[132px]" allowAnimate={false} alt="SOAFEE Homepage"></SOAFEELogo>
        </div>
        <div className="md:basis-1/4 px-4 border-l-2 mb-10 md:mb-0 border-soafee-yellow pb-28">
          <div className="flex flex-col">{menu(footerMenu)}</div>
        </div>
        <div className="md:basis-1/4 pb-28 px-4 border-l-2 border-soafee-yellow text-white">
          <div className="text-soafee-yellow typography-desktop-b2 md:typography-desktop-b3 mb-5">
            <a href="mailto:info@soafee.io">Contact</a>
          </div>
          <p className="typography-desktop-b4 mb-7">
            If you would like to learn more or support and contribute to the SOAFEE project, please contact us.
          </p>
          <span className="font-mono font-[500] text-[14px] leading-6 text-soafee-mid-gray">
            <p>© 2025 SOAFEE</p>
            <p>ALL RIGHTS RESERVED</p>
          </span>
        </div>
      </div>
    </div>
  );
};

export default Footer;
