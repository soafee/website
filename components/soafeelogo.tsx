import SOAFEE from '/public/soafee.svg';

type Props = {
  allowAnimate?: boolean;
  className?: string;
  style?: string;
  alt?: string;
};

const SOAFEELogo = (props: Props) => {
  let mouseEnter = (e: Event) => {
    if (props?.allowAnimate) {
      (e.target as Element).classList.add('animate');
    }
  };

  let mouseLeave = (e: Event) => {
    if (props?.allowAnimate) {
      (e.target as Element).classList.remove('animate');
    }
  };

  return (
    <SOAFEE
      className={`soafee-logo ${props.className || ''}`}
      alt={props.alt || ''}
      style={props.style}
      onMouseEnter={mouseEnter}
      onMouseLeave={mouseLeave}
    />
  );
};

export default SOAFEELogo;
