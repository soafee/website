import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
};

const BlogContainer = ({ children }: Props) => {
  return (
    <section className="bg-soafee-light-gray">
      <div className="wrapper-lg py-14 lg:py-28">
        <div className="flex flex-col lg:flex-row justify-between items-start">{children}</div>
      </div>
    </section>
  );
};

BlogContainer.title = ({ children }: Props) => {
  return <div className="flex-1 max-w-[700px] mb-5">{children}</div>;
};

BlogContainer.body = ({ children }: Props) => {
  return (
    <div className="flex flex-1 max-w-[600px] justify-end">
      <div className="w-full lg:w-4/5 typography-mobile-b2 lg:typography-desktop-b3">{children}</div>
    </div>
  );
};

export default BlogContainer;
