import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
  className?: string;
};

const HorizontalScrollList = ({ children, className }: Props) => {
  return (
    <div className={className} style={{ overflowX: 'auto', marginRight: '0!important' }}>
      {children}
    </div>
  );
};

export default HorizontalScrollList;
