import { FC } from 'react';
import Image, { type StaticImageData } from 'next/image';
import Link from 'next/link';
import Border from './border';
import RegisterCard from '@/components/register-card';

interface HeroItemProps {
  desktopTitle: string;
  desktopBody: string;
  mobileTitle: string;
  ctaUrl: string;
  image: StaticImageData;
}

const HeroItem: FC<HeroItemProps> = ({ desktopTitle, desktopBody, mobileTitle, ctaUrl, image }) => {
  return (
    <div className="flex flex-col">
      <div className="h-[580px] mx-0 flex flex-col md:flex-row pt-20 md:pt-[102px] justify-between items-start font-aspekta">
        <div className="wrapper-lg align-self-center max-w-[900px] pr-[4%] md:pr-0">
          <h1 className="hidden md:block pt-5 mb-4 text-soafee-black">{desktopTitle}</h1>
          <h1 className="md:hidden mb-4 text-soafee-black"> {mobileTitle}</h1>
          <div className="typography-b1 text-soafee-black mb-6 flex max-w-[580px]">{desktopBody}</div>
          <Link href={ctaUrl}>
            <Border className="bg-white">Learn More</Border>
          </Link>
        </div>
        <RegisterCard />
      </div>
      <Image
        priority
        className="block h-[390px] w-full object-cover object-left"
        src={image}
        alt={`${desktopTitle} hero background`}
      />
    </div>
  );
};

export default HeroItem;
