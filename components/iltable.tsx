import Link from 'next/link';

type ILColumnType = {
  key: string;
  header: string;
};

type ILRowType = {
  suitename: string;
};

const suitenames = new Map([
  ['Build', 'build'],
  ['Boot', 'smoke-ls'],
  ['Soafee test suite', 'soafee-test-suite']
]);

type ILTableRowsProps = {
  rows: Array<ILRowType>;
  columns: Array<ILColumnType>;
  in_testruns: Array<string>;
};

const ILTableRows = ({ rows, columns, in_testruns }: ILTableRowsProps): JSX.Element => {
  var testruns = new Map();
  for (var tr of in_testruns) {
    testruns.set(tr, 'defined');
  }
  const rowsrendered = rows.map((row, index) => {
    return (
      <tr key={`row-${index}`}>
        {columns.map((column, index2) => {
          return column.key == 'testsuite' ? (
            <td key={`cell-${index2}`}>{row.suitename}</td>
          ) : (
            <td key={`cell-${index2}`}>
              {testruns.get(column.key) != undefined ? (
                <Link
                  href={
                    'https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests/?environment=' +
                    (column.key == 'ava-xen' && row.suitename == 'Build' ? 'qemu' : column.key) +
                    '&suite=' +
                    suitenames.get(row.suitename)
                  }
                >
                  <img
                    src={
                      'https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=' +
                      (column.key == 'ava-xen' && row.suitename == 'Build' ? 'qemu' : column.key) +
                      '&suite=' +
                      suitenames.get(row.suitename) +
                      '&passrate&title&hide_zeros=1'
                    }
                    alt=""
                  />
                </Link>
              ) : (
                'N/A'
              )}
            </td>
          );
        })}
      </tr>
    );
  });

  return <tbody>{rowsrendered}</tbody>;
};

type ILTableHeaderProps = {
  columns: Array<ILColumnType>;
};

const ILTableHeader = ({ columns }: ILTableHeaderProps): JSX.Element => {
  const headers = columns.map((column, index) => {
    return <th key={`headCell-${index}`}>{column.header}</th>;
  });

  return (
    <thead>
      <tr>{headers}</tr>
    </thead>
  );
};

type ILTableProps = {
  rows: Array<ILRowType>;
  columns: Array<ILColumnType>;
  testruns: Array<string>;
};

const ILTable = ({ rows, columns, testruns }: ILTableProps): JSX.Element => {
  return (
    <table>
      <ILTableHeader columns={columns} />
      <ILTableRows rows={rows} columns={columns} in_testruns={testruns} />
    </table>
  );
};

export { type ILRowType, type ILColumnType };
export default ILTable;
