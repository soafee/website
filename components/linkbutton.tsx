type Props = {
  url: string;
  children: React.ReactNode;
};

let LinkButton = (props: Props) => {
  return (
    <a href={props.url}>
      <button style={{ width: '100%' }}>{props.children}</button>
    </a>
  );
};

export default LinkButton;
