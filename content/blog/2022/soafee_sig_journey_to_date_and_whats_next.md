---
Title: SOAFEE SIG’s Journey To Date And What’s Next
Description: Read about SOAFEE SIG’s journey and get a glimpse into what’s to come
Date: 2022-08-22
Banner:
  Active: true
  Title: SOAFEE SIG’s Journey To Date And What’s Next
  Description: Read about SOAFEE SIG’s journey and get a glimpse into what’s to come
  Background: banner/banner1.jpg
---

# SOAFEE SIG’s Journey To Date And What’s Next

It’s been 1 year since The Scalable Open Architecture For Embedded Edge (SOAFEE) Special Interest Group (SIG) was launched in September 2021. It’s been an amazing journey to date as the concepts like cloud-native in automotive that have been talked about for some time now are now becoming a reality in the automotive industry. In this blog, we will go through what we as SOAFEE SIG have achieved till date from its launch and give a view on what the upcoming short term plans are for the automotive industry to participate in. It is going to be a very exciting journey indeed!

{{< button url="https://drive.google.com/file/d/1XAirUTWWuDYVAZku947m3EUYOOJMC1vw/view" text="Read the Blog Post">}}
