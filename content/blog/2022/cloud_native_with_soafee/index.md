---
Title: Cloud-native Development with SOAFEE
Description: AWS and Arm demonstrate the benefits of using similar processing in cloud and car  - environmental execution parity
Date: 2022-03-30
Banner:
  Active: false
  Title: Cloud-native Development with SOAFEE
  Description: AWS and Arm demonstrate the benefits of using similar processing in cloud and car  - environmental execution parity
  Background: banner/banner5.jpg
---

# Arm and AWS Demonstrate Cloud-native Automotive Development with SOAFEE

{{< img src="images/car.webp" alt="car">}}

Enabling a cloud-native methodology for workload development for the software defined vehicle means the right processing in both the cloud and the car, along with the right software frameworks to facilitate it.

In this blog and demo we show how software can be developed on AWS graviton cloud instances before being deployed to similar computing frameworks in the vehicle, thus reducing the complexity, cost, risk and time of cross compiling code.

{{< button url="https://www.arm.com/blogs/blueprint/cloud-native-automotive-development" text="Read the blog">}}


