---
Title: Getting Started with Cloud-Native Automotive Software Development
Description: An Automotive Software Development workshop with Arm and AWS
Date: 2022-06-16
Banner:
  Active: trfalseue
  Title: Getting Started with Cloud-Native Automotive Software Development
  Description: A workshop introducing an automotive-native development infrastructure for containerized workloads with environmental parity across cloud and edge
  Background: banner/banner1.jpg
---

# Getting Started with Cloud-Native Automotive Software Development

This workshop will introduce a novel automotive-native software development infrastructure able to execute the same containerized workload - with __environmental parity__ - in all the targeted compute elements: an [AWS EC2 Graviton2](https://aws.amazon.com/ec2/graviton/) instance, a [Raspberry Pi](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/), and an [AVA Developer Platform](https://www.adlinktech.com/en/soafee).

As defined by [Kevin Hoffman in his book](https://www.oreilly.com/content/environment-parity-for-rapidly-deployed-cloud-native-apps/):

>  The purpose of [...] environment parity is to give your team and your entire organization the confidence that the application will work everywhere.

This is a major element in achieving the objective of [_automotive_ cloud-native software-defined vehicles](https://community.arm.com/developer/ip-products/system/b/embedded-blog/posts/cloud-native-approach-to-the-software-defined-car).

You will learn how to utilize AWS to create a CI/CD pipeline that builds, containerizes, evaluates, and enables deployment - at scale in the cloud and on embedded devices - of a perception network, [YOLO](https://pjreddie.com/darknet/yolo/). This network is used as a stand-in for any automotive application workload to demonstrate the design paradigm. The specific version of YOLO used in this workshop is the [one implemented in the Autoware stack](https://github.com/autowarefoundation/modelzoo/tree/master/perception/camera_obstacle_detection/yolo_v2_tiny/tensorflow_fp32_coco) (YOLOv2-Tiny), running on Ubuntu Linux 20.04.


The full System Under Test (SUT) stack will include the Operating System (a Yocto-Linux distribution) and Arm's [Edge Workload Abstraction and Orchestration Layer (EWAOL)](https://gitlab.com/soafee/ewaol).

{{< button url="https://catalog.us-east-1.prod.workshops.aws/workshops/12f31c93-5926-4477-996c-d47f4524905d/en-US/" text="Continue to the Workshop">}}
