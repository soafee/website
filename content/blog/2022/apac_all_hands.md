---
Title: SOAFEE Live Q&A Session
Description: On Wednesday 6th April at 4pm Pacific Daylight Time, members of the SOAFEE Special Interest Group will be hosting a live session to answer questions about the SOAFEE initiative.
Date: 2022-03-29
Banner:
  Active: false
  Title: SOAFEE Live Q&A Session
  Description: On Wednesday 6th April at 4pm Pacific Daylight Time, members of the SOAFEE Special Interest Group will be hosting a live session to answer questions about the SOAFEE initiative.
  Background: banner/banner5.jpg
---

# SOAFEE – live Q&A session

On Wednesday 6th April at 4pm Pacific Daylight Time, members of the SOAFEE Special Interest Group will be hosting a live session to answer questions about the SOAFEE initiative.

This session is primarily aimed at interested parties who could not attend the live all-hands meeting in January, but have [watched the recording](https://soafee.io/blog/2022/all_hands_recording/) and have questions on the SOAFEE initiative.

Note, we will not be repeating the all-hands presentation, so please watch the recording before attending the live Q&A session.

{{< button url="https://armltd.zoom.us/webinar/register/WN_mCVolaVBTRyiOTqrSf7Qdg" text="To register for the Q&A – please click here">}}
