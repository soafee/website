---
Title: Using SystemReady in Automotive Applications
Description: A presentation from Arm and Continental about the value of SystemReady in Automotive Applications
Date: "2022-06-01"
Banner:
  Active: false
  Title: Using SystemReady in Automotive Applications
  Description: A presentation from Arm and Continental about the value of SystemReady in Automotive Applications
  Background: banner/banner1.jpg
---

# SystemReady in Automotive Applications

The software content of vehicles is increasing as the industry moves towards the vision of software defined vehicles. Portability of operating system software is important between different vehicles and different development environments. Additionally, long maintenance cycles create risk in supporting platform-specific software, particularly in an open source environment. This session will look at the role SystemReady will play in addressing these challenges.

{{< button url="https://resources.linaro.org/en/resource/okjomGVLS1hQHZ44hmZ5Cv" text="Watch the Presentation">}}
