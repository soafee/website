---
Title: Mobica
Description: Mobica member bio
Date: 2022-10-06
Member: voting/mobica
---


{{< member_header >}}
{{</ member_header >}}

## About Mobica

Mobica provides software engineering and development capabilities to businesses
building amazing products and services for tomorrow’s world.
With 18 years of technology innovation and centres across the EU, UK and USA and
more than 700 highly skilled and experienced engineers, we can provide access to
something that’s hard to find: diverse development talent across the tech stack.
Our engineers provide expertise across the entire software stack, to take you from
development of embedded systems and cloud based back-end solutions, through to
front-end web and mobile applications.
For more information on Mobica’s services, visit www.mobica.com or follow us on
LinkedIn, Twitter, Facebook and Instagram.
