---
Title: TTTech Auto
Description: TTTech Auto member bio
Date: 2022-05-03
Member: voting/tttech_auto
---


{{< member_header >}}
{{</ member_header >}}

## About TTTech Auto

TTTech Auto is a future-oriented auto-tech company providing advanced automotive solutions and driving safe autonomous mobility for everyone. It operates under the umbrella of TTTech Group, a technology leader in robust networking and safety controls with over 20 years of experience.

TTTech Auto has developed MotionWise, the first series-proven safe vehicle software platform on the market. MotionWise complies with the ISO 26262 safety standard to ensure safety at all stages of the vehicle software development lifecycle - from concept to development to deployment and updates.

We are pleased to bring our extensive experience in developing safe real-time platforms to SOAFEE.

