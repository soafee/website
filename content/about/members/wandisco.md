---
Title: WANdisco
Description: WANdisco Member Bio
Date: 2022-09-15
Member: voting/wandisco
---

{{< member_header >}}
{{</ member_header >}}

## About WANdisco

WANdisco is the first and only data activation platform for accelerating digital transformation at scale. WANdisco makes infinite data actionable across clouds and enterprises in real time. WANdisco customers unleash the business value of the cloud with zero downtime, data loss, or disruption to fuel AI and machine learning, create new services, and transform businesses.
