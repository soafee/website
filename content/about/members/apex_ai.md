---
Title: Apex.AI
Description: Apex.AI Member Bio
Date: 2022-11-04
Member: voting/apexai
---


{{< member_header >}}
{{</ member_header >}}

## About Apex.AI

Apex.AI provides safety-certified products enabling the transformation to the software-defined future of the automotive industry. Our products Apex.OS and Apex.Middleware are implementing cloud connectivity and the runtime environment to orchestrate automotive applications in mixed-critical systems. Apex.AI will bring its complete competence to take the next step toward software-defined vehicles.

