---
Title: Magna joins SOAFEE SIG
Description: New member announcement – click for more details on Magna
Date: 2022-10-24
Member: voting/magna
---

{{< member_header >}}
{{</ member_header >}}

## About Magna

Magna is more than one of the world’s largest suppliers in the automotive space. We are a mobility technology company with a global, entrepreneurial-minded team of 161,000 employees and an organizational structure designed to innovate like a startup. With 65+ years of expertise, and a systems approach to design, engineering and manufacturing that touches nearly every aspect of the vehicle, we are positioned to support advancing mobility in a transforming industry. Our global network includes 341 manufacturing operations and 89 product development, engineering and sales centers spanning 28 countries.
