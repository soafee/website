---
Title: Marvell
Description: Marvell Member Bio
Date: 2022-09-29
Member: voting/marvell
---


{{< member_header >}}
{{</ member_header >}}

## About Marvell

To deliver the data infrastructure technology that connects the world, we’re building solutions on the most powerful foundation: our partnerships with our customers. Trusted by the world’s leading technology companies for over 25 years, we move, store, process and secure the world’s data with semiconductor solutions designed for our customers’ current needs and future ambitions. Through a process of deep collaboration and transparency, we’re ultimately changing the way tomorrow’s enterprise, cloud, automotive, and carrier architectures transform—for the better.
