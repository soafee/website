---
Title: TUM
Description: TUM Member Bio
Date: 2022-06-28
Member: voting/tum
---

{{< member_header >}}
{{</ member_header >}}

## About Technical University of Munich
The Technical University of Munich (TUM) is one of Europe’s leading research universities,
with more than 600 professors, 48,000 students, and 11,000 academic and non-academic
staff. Its focus areas are the engineering sciences, natural sciences, life sciences and
medicine, combined with economic and social sciences. TUM acts as an entrepreneurial
university that promotes talents and creates value for society. In that it profits from having
strong partners in science and industry. It is represented worldwide with the TUM Asia
campus in Singapore as well as offices in Beijing, Brussels, Mumbai, San Francisco, and
São Paulo. Nobel Prize winners and inventors such as Rudolf Diesel, Carl von Linde, and
Rudolf Mößbauer have done research at TUM. In 2006, 2012, and 2019 it won recognition as
a German "Excellence University." In international rankings, TUM regularly places among the
best universities in Germany.

