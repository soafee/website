---
Title: Sensory
Description: Sensory Member Bio
Date: 2022-09-13
Member: voting/sensory
---

{{< member_header >}}
{{</ member_header >}}

## About Sensory

Sensory Inc. creates a safer and superior UX through vision and voice technologies. Sensory technologies are widely deployed in consumer electronics applications including mobile phones, automotive, wearables, toys, IoT, PCs, medical products, and various home electronics. Sensory’s product line includes TrulyHandsfree voice control, TrulySecure biometric authentication, TrulyNatural large vocabulary natural language embedded speech recognition and SensoryCloud.ai, a complete AI as a Service platform designed for processing voice and vision AI workloads in the cloud. Sensory’s technologies have shipped in over three billion units of leading consumer products.

Websites:
- www.sensory.com
- www.sensorycloud.ai

Social Media:
- https://twitter.com/trulyhandsfree
- http://www.youtube.com/c/SensoryInc
- https://www.linkedin.com/company/sensory-inc-/
