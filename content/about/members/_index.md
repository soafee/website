---
Title: SOAFEE Members
layout: single
---

## Governing Body Members

{{< imagegrid columns="3" data="members.governing_body" media_page="headless_media" >}}

---

## Voting Members

{{< imagegrid columns="6" data="members.voting" media_page="headless_media" >}}
