---
Title: ThunderSoft
Description: ThunderSoft member bio
Date: 2022-04-26
Member: voting/thundersoft
---


{{< member_header >}}
{{</ member_header >}}

## About ThunderSoft

ThunderSoft, the world leading intelligent operating system products and technologies provider, has been continuously accumulating and innovating in the operating system field, with its business expanding gradually from smart terminals to AIoT, smart vehicle and smart industries. It was successfully listed in 2015, which leads it to be China's first listed technical company specialized in the intelligent operating system. Till now, the subsidiaries and R&D centers of ThunderSoft have covered 40 cities around the world, with a total of over 11,000 employees and 90% of them are engineers.

## What ThunderSoft can bring to the SOAFEE SIG

ThunderSoft is excited to bring our decades of series experience in the automotive industry, our solutions in intelligent cockpit, auto OS, ADAS, development toolchains, and our cross-industry expertise from AI, IoT, Edge, Telecom and HPC to SOAFEE. ThunderSoft will leverage the new E/E system and Cloud-native architecture, develop a SOAFEE based reference software stack with our intelligent cockpit and ADAS workloads. Together we strive to promote SOAFEE and this reference to our ecosystem partners and our customers.
