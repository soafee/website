---
Title: "Siemens joins SOAFEE SIG"
Description: "New member announcement – click for more details on Siemens"
Date: "2022-08-05"
Member: "voting/siemens"
---


{{< member_header >}}
{{</ member_header >}}

## About Siemens

Siemens Digital Industries Software is driving transformation to enable a digital enterprise where engineering, manufacturing and electronics design meet tomorrow. The Xcelerator portfolio helps companies of all sizes create and leverage digital twins that provide organizations with new insights, opportunities, and levels of automation to drive innovation. For more information on Siemens Digital Industries Software products and services, visit www.sw.siemens.com or follow us on LinkedIn, Twitter, Facebook and Instagram. Siemens Digital Industries Software – Where today meets tomorrow.