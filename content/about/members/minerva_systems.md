---
Title: Minerva Systems
Description: Minerva Systems Bio
Date: 2022-06-14
Member: voting/minerva_systems
---


{{< member_header >}}
{{</ member_header >}}


## About Minerva Systems

Minerva Systems designs and develops embedded system software and
development tools for future autonomous systems, especially in the
automotive domain. The optimised design and the simplified deployment of
Minerva Systems solutions enable scalable construction and delivery of
predictable, multiple-criticality AI-based applications exploiting the
next generation of high-performance embedded computers. Minerva Systems
brings freedom-from-interference by design.

Minerva Systems is a startup funded at the end of 2021, spinoff of the
University of Modena and Reggio Emilia. It operates in Modena (IT) and
Munich (DE) and is supported by a large scientific advisory board
bringing together world-renowned professors in real-time and embedded
systems.
