---
Title: NetFoundry joins SOAFEE SIG
Description: New member announcement – click for more details on NetFoundry
Date: 2022-05-17
Member: voting/netfoundry
---


{{< member_header >}}
{{</ member_header >}}

## About NetFoundry

NetFoundry reinvents the network, enabling businesses to innovate faster, ensure security-by-default, massive simplicity, and reduced costs. 

Built on opensource OpenZiti, it is the only solution purpose-built to connect massively distributed apps, edges, clouds, IoT, and devices in minutes, ensuring zero trust of the internet, local and OS host network. By isolating the app from the network, we remove the pain, cost and complexity of public DNS, VPNs, bastions, complex firewall rules, custom hardware, and private circuits.

The NetFoundry SaaS platform, is accessed via GUI, APIs, SDKs, and DevOps tools integrations, enabling everyone to benefit from connectivity-as-code. The company is headquartered in Charlotte, North Carolina, with offices across the globe. Getting started is easy and free. Select your desired package now - https://netfoundry.io/pricing/

