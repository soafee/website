---
Title: AMD
Description: AMD member bio
Date: 2022-07-27
Member: voting/amd
---


{{< member_header >}}
{{</ member_header >}}

## About AMD

For more than 50 years AMD has driven innovation in high-performance computing, graphics and visualization technologies ― the building blocks for gaming, immersive platforms and the datacenter. Hundreds of millions of consumers, leading Fortune 500 businesses and cutting-edge scientific research facilities around the world rely on AMD technology daily to improve how they live, work and play. AMD employees around the world are focused on building great products that push the boundaries of what is possible. For more information about how AMD is enabling today and inspiring tomorrow, visit the AMD (NASDAQ: AMD) [website](https://www.amd.com/), [blog](https://community.amd.com/), [Facebook](https://www.facebook.com/AMD) and [Twitter](https://twitter.com/amd) pages.