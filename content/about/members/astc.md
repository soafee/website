---
Title: ASTC
Description: ASTC Member Bio
Date: 2022-11-02
Member: voting/astc
---


{{< member_header >}}
{{</ member_header >}}

## About ASTC & VLAB Works

VLAB Works, an ASTC company, is an industry leader in software technology for the modelling, simulation, and virtual prototyping of embedded electronic systems. VLAB Works customers apply this technology to the virtualisation and automation of the embedded system development process. We help replace hardware with software, paper specifications with live simulations, late with early, slow with fast, limited with unlimited, complex with simple, and manual with automated.

VLAB Works technology and solutions enable production-level agile embedded development processes. These processes help accelerate product development schedules, develop optimised software and systems, improve quality, reduce risk, and eliminate the need for physical lab setups and hardware. VLAB Works helps customers design better products, do a better job in development, reduce the need for manufacturing and hardware, and help keep our planet greener.