---
Title: Wipro
Description: Wipro member bio
Date: 2022-03-29
Member: voting/wipro
---


{{< member_header >}}
{{</ member_header >}}

## About WIPRO

Wipro Limited (NYSE: WIT, BSE: 507685, NSE: WIPRO) is a leading global information technology, consulting and business process services company. We harness the power of cognitive computing, hyper-automation, robotics, cloud, analytics and emerging technologies to help our clients adapt to the digital world and make them successful. A company recognized globally for its comprehensive portfolio of services, strong commitment to sustainability and good corporate citizenship, we have over 220,000 dedicated employees serving clients across six continents. Together, we discover ideas and connect the dots to build a better and a bold new future.

## What Wipro can bring to the SOAFEE SIG

In March 2022, Wipro unveiled its "Cloud Car" Platform initiative, that aims to bring together its broad services across automotive value chain to deliver automotive engineering capabilities with a best-in-class partner ecosystem and enabling with industry supported standards/consortiums. Our platform approach shall provide automakers with a technically elegant - Integrated, Robust architecture solution with in-vehicle software defined control & data layer services enabled with cloud-native support. Along with this platform eco-system, Wipro aims to integrate an end-to-end cybersecurity prefixed Software Defined Vehicle system with advanced features that can address multi-segmented needs. The platform will enable mixed criticality and inter-platform operable workloads with SOA & Microservices bringing innovative technology at a faster pace with at a lower CAPEX & OPEX model. We believe that in collaboration Scalable Open Architecture for Embedded Edge (SOAFEE) initiative ecosystem partners, we can deliver our reference platform for automotive industry to bring safety and real-time aware cloud-native paradigms to automotive development, keeping software-defined vehicles (SDVs) digitally relevant for years to come. Wipro aims to be a #1 Industry leader when it comes to enabling & transforming Software Defined Automotive domain markets.
