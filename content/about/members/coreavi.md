---
Title: CoreAVI
Description: CoreAVI member bio
Date: 2022-08-01
Member: voting/coreavi
---


{{< member_header >}}
{{</ member_header >}}

## About CoreAVI

CoreAVI is the global leader in architecting and delivering safety critical graphics and compute software drivers and libraries, embedded ‘system on chip’ and discrete graphics processor components, and certifiable platform hardware IP. CoreAVI’s comprehensive software suite enables development and deployment of complete safety critical solutions for automotive, industrial and aerospace applications requiring certification to the highest integrity levels coupled with full lifecycle support. CoreAVI’s solutions support both graphics and compute applications including safe autonomy, machine vision and AI in the automotive, unmanned vehicle and industrial IoT markets, as well as commercial and military avionics systems. www.coreavi.com