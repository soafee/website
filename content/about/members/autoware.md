---
Title: Autoware
Description: Autoware Member Bio
Date: 2022-11-15
Member: voting/autoware
---


{{< member_header >}}
{{</ member_header >}}

## About Autoware

Autoware is the world’s first “all-in-one” open-source software for self-driving vehicles hosted under the Autoware Foundation. Autoware is based on ROS 2, with contributions from the Autoware Foundation members and community at large following best-in-class software engineering practices. Further information at www.autoware.org.