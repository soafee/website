---
Title: MIH joins SOAFEE SIG
Description: New member announcement – click for more details on MIH
Date: 2022-06-08
Member: voting/mih
---


{{< member_header >}}
{{</ member_header >}}

## About MIH

MIH Consortium is creating an open EV ecosystem that promotes collaboration in the mobility industry. Our mission is to realize key technologies, develop reference designs and standards, while we bridge the gap for alliance members resulting in a lower barrier to entry, accelerated innovation, and shorter development cycles. Our goal is to bring together strategic partners to create innovative solutions for the next generation of EV, autonomous driving, and mobility service applications.
