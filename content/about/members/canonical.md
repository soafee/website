---
Title: Canonical
Description: Canonical Member Bio
Date: 2022-12-06
Member: voting/canonical
---

{{< member_header >}}
{{</ member_header >}}

## About Canonical

Canonical, established in 2004, is the company behind [Ubuntu](https://ubuntu.com/), the leading OS for container, cloud, scale-out and hyperscale computing. Canonical provides enterprise support and services for commercial users of Ubuntu. From smart homes to smart drones, robots, and industrial systems, Ubuntu is also the new standard for embedded Linux. Furthermore, Ubuntu is the reference platform for Kubernetes on all major public clouds. And Canonical MicroK8s delivers a lightweight, fully-featured, conformant Kubernetes for IoT devices. Additionally, 65% of large-scale OpenStack deployments are on Ubuntu, using both KVM and the pure-container LXD hypervisor for the world's fastest private clouds. Canonical also leads the development of Juju, the model-driven operations system, and MAAS (Metal-as-a-Service), which creates a physical server cloud and IP address management for amazing data center operational efficiency.