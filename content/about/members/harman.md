---
Title: HARMAN
Description: HARMAN member bio
Date: 2022-07-26
Member: voting/harman
---

{{< member_header >}}
{{</ member_header >}}

## About HARMAN

[HARMAN](https://www.harman.com) designs and engineers connected products and solutions for automakers, consumers, and enterprises worldwide, including connected car systems, audio and visual products, enterprise automation solutions; and services supporting the Internet of Things.

Our HARMAN Automotive Division is dedicated to the opportunities arising from the emergence of the Software Defined Vehicle (SDV). Focused on the in-cabin experience, we are delivering consumer experiences at automotive grade to help carmakers redefine the value proposition of the SDV with the consumer in mind.
