---
Title: Lightfleet
Description: Lightfleet member bio
Date: 2022-04-07
Member: voting/lightfleet
---

{{< member_header >}}
{{</ member_header >}}

## About Lightfleet

Lightfleet has developed a network fabric that will bring unparalleled levels of performance, redundancy, and reliability to the autonomous vehicle platform. Lightfleet Multiflo™ is a revolutionary network that has zero skew, zero jitter and deterministically delivers all packets simultaneously to one or multiple end points. Multiflo is a hardware-based network implementation that does not have any software stack. Multiflo promises to bring a new level of network functionality and safety to the autonomous vehicle platform of the future!

Lightfleet joined the SOAFEE SIG to partner with other members of the SIG to develop the safest and most reliable autonomous vehicles possible!

 