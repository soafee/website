---
Title: CoAsia Nexell joins SOAFEE SIG
Description: New member announcement – click for more details on CoAsia Nexell
Date: 2022-12-13
Member: voting/coasia_nexell
---


{{< member_header >}}
{{</ member_header >}}

## About CoAsia Nexell

CoAsia Nexell is a Fabless SoC and a SoC design service company that has been introduced own branded Application Processor (AP) SoC to Automotive infotainment and IoT application markets and it also has been successfully developed and delivered multiple custom designed ASIC SoC service to customers with latest Linux, Android OS BSP and evaluation hardware PCB.

Benefit and strength that CoAsia Nexell provides to partners and customers is, it’s market proven, configurable SoC platforms which have embedded own GPU (Graphic Processor Unit) and various IPs as like latest CPU, multimedia codec, memory controllers and related software development environments as like evaluation boards and device drivers for multiple OS provide best Price-performance competitiveness and time to market opportunities.