---
Title: Driveblocks joins SOAFEE SIG
Description: New member announcement – click for more details on Driveblocks
Date: 2022-05-19
Member: voting/driveblocks
---


{{< member_header >}}
{{</ member_header >}}

## About Driveblocks

driveblocks develops a modular and scalable software stack for autonomous driving. It’s focus area are heavy commercial vehicles: yard logistics, mining, agriculture and hub-to-hub transportation. The unique combination of open-source technologies, an open-architecture design and cutting edge algorithms enables the integration of the software modules on various customer hardware and software platforms. The heavily modular design allows the re-combination of modules to solve even the most challenging use-cases and the seamless integration with existing autonomy functionality while considering safety and certification.

The Munich based startup was founded by members of the Indy Autonomous Challenge winning team of the Technical University of Munich. The team has a strong track-record of solving real-world challenges in autonomous driving at the limits of technology by using a highly iterative and heavily simulation-driven development workflow and engaging with the open-source community.
