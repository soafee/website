---
Title: TATA ELXSI joins SOAFEE SIG
Description: New member announcement – click for more details on TATA ELXSI
Date: 2022-11-29
Member: voting/tata_elxsi
---


{{< member_header >}}
{{</ member_header >}}

## About TATA ELXSI

Incorporated in 1989, Tata Elxsi is amongst the world’s leading providers of design and technology services across industries including Automotive, Media, Communications, Healthcare and Transportation Tata Elxsi works with leading OEMs and suppliers in the automotive and transportation industries for R&D, design, and product engineering services from architecture to launch and beyond.

It brings together domain experience across Autonomous, Electric, Connected vehicle technologies, Software-defined vehicles (SDV ) and is supported by a worldwide network of design studios, development centers and offices as well as a global pool of over 12,000 engineers and specialists. For more information, please [visit](https://www.tataelxsi.com/industries/automotive)


