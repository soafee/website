---
Title: LG Electronics
Description: LG Electronics member bio
Date: 2022-04-05
Member: voting/lge
---

{{< member_header >}}
{{</ member_header >}}

## About LG Electronics

LGE focuses on innovation and preparing for the future, knowing that diversification and evolution is necessary to respond to the tides of change.

And it is also developing advanced auto parts including cockpit electronics, connectivity solutions and automotive vision systems for the fast growing automotive industry.

LGE strongly believes that cloud native technology will be key feature in future automotive eco systems and SDV for providing new experiences and services. 

LGE has been developing its own platform for SDV capable of Continuous Integration/Continuous Deployment, containerized application and OS.

It could be a exciting journey for collaborating solutions about service orchestration between cloud and embedded edge in a mixed critical environment with ARM SOAFEE SIG members.