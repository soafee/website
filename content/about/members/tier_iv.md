---
Title: TIER IV
Description: TIER IV member bio
Date: 2022-03-15
Member: voting/tieriv
---

{{< member_header >}}
{{</ member_header >}}

## About TIER IV

TIER IV is the creator of [Autoware](https://www.autoware.org/), the world’s first open-source software for autonomous driving, and provides Autoware-based full-stack solutions for the commercialization of autonomous vehicles.

“TIER IV values open-source software and is working with the Autoware Foundation members to develop the Open AD kit based on SOAFEE  as a reference framework to accelerate the AD software development for software-defined vehicles. We look forward to working together with SOAFEE members to enable cloud-native software development for autonomous vehicles."  -  Shinpei Kato, Founder & CTO, TIER IV, Co-Founder & Chairman, The Autoware Foundation.