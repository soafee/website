---
headless: true
---

This is a headless page resource.

https://gohugo.io/content-management/page-bundles/#headless-bundle

It will not be published, but is a page bundle that
can be referenced by other pages.