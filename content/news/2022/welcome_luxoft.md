---
Title: "Luxoft joins SOAFEE SIG"
Description: "New member announcement – click for more details on Luxoft"
Date: "2022-04-21"
Member: "voting/luxoft"

Banner:
  Active: false
  Title: Luxoft joins SOAFEE SIG
  Description: Luxoft joins the SOAFEE SIG as a voting member.
  Background: "banner/banner2.jpg"

Card:
  Class: new-member
---


{{< member_header >}}
# Luxoft joins SOAFEE SIG

The SOAFEE community are excited to welcome Luxoft as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Luxoft

Luxoft is the design, data and development arm of [DXC Technology](https://dxc.com/), providing bespoke, end-to-end technology solutions for mission critical systems, products and services.

We help create data-fueled organizations, solving complex operational, technological and strategic challenges.

Our passion is building resilient businesses, while generating new business channels and revenue streams, exceptional user experiences and modernized operations at scale.

With expertise spanning Autonomous Drive, Connected Mobility, Digital Cockpit, UX, Electric and Testing and Validation, Luxoft Automotive helps automakers and their key partners deliver the shared, personal, connected and intelligent mobility of the future, today.

## Luxoft’s contribution to the SOAFEE SIG

Luxoft has a strong track record working with organizations to drive standardization and believes in the value of a strong partner eco system building on standards and reusable assets.

“Luxoft is excited to be part of the SOAFEE Special Interest Group and support the automotive Industry’s transformation towards Software Defined Vehicles.” said Damian Barnett, CTO Luxoft Automotive.

“This is a fantastic opportunity to bring together the work we already do to help shape the industry’s vision of a software defined vehicle.”

“We are looking forward to bringing our automotive experience and expertise, from in-vehicle, development back end and cloud to help accelerate the industry’s journey towards Software Defined Vehicles”
