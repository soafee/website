---
Title: Marvell joins SOAFEE SIG
Description: New member announcement – click for more details on Marvell
Date: 2022-09-29
Member: voting/marvell

Banner:
  Active: false
  Title: Marvell joins SOAFEE SIG
  Description: Marvell joins the SOAFEE SIG as a voting member.
  Background: banner/banner2.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# Marvell joins SOAFEE SIG

The SOAFEE community are excited to welcome Marvell as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Marvell

To deliver the data infrastructure technology that connects the world, we’re building solutions on the most powerful foundation: our partnerships with our customers. Trusted by the world’s leading technology companies for over 25 years, we move, store, process and secure the world’s data with semiconductor solutions designed for our customers’ current needs and future ambitions. Through a process of deep collaboration and transparency, we’re ultimately changing the way tomorrow’s enterprise, cloud, automotive, and carrier architectures transform—for the better.

## Why Marvell is joining SOAFEE SIG

Marvell joins SOAFEE with the goal of applying its expertise in cloud-optimized silicon to accelerating the application of the cloud-native paradigm to the software-defined vehicle. Together with its technology partners, the open-standards community, and now the SOAFEE SIG, Marvell is helping to shape the platforms and architecture needed to power next-generation SDV workloads, both in-vehicle and in the cloud.