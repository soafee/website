---
Title: Ampere joins SOAFEE SIG
Description: New member announcement – click for more details on Ampere
Date: 2022-10-20
Member: voting/ampere

Banner:
  Active: false
  Title: Ampere joins SOAFEE SIG
  Description: Ampere joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# Ampere joins SOAFEE SIG

The SOAFEE community are excited to welcome Ampere as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why Ampere are joining SOAFEE

Ampere joins SOAFEE with the goal of bringing its Cloud Native expertise to the entire software-defined vehicle ecosystem. Ampere’s cloud native Arm-based processors give parity with vehicle ECUs for software development, testing and simulation. This simplifies the software toolchain, eliminates cross-compilation and ends cross-platform debugging, which results in higher quality software while saving energy. This is why SOAFEE’s [reference development platform](https://solutions.amperecomputing.com/systems/altra/kraken-comhpc-WS) uses Ampere.

## About Ampere

Ampere is a modern semiconductor company designing the future of cloud computing with the world's first Cloud Native Processors. Ampere’s CPUs are the solution for building a sustainable cloud by delivering both the highest performance and efficiency for cloud computing applications. With Ampere’s Cloud Native Processors, cloud compute can achieve new levels of scale in any location while also reducing the environmental footprint.

For more information visit https://amperecomputing.com. 