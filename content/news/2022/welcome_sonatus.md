---
Title: "Sonatus joins SOAFEE SIG"
Description: "New member announcement – click for more details on Sonatus"
Date: "2022-04-19"
Member: "voting/sonatus"

Banner:
  Active: false
  Title: Sonatus joins SOAFEE SIG
  Description: Sonatus joins the SOAFEE SIG as a voting member.
  Background: "banner/banner4.jpg"

Card:
  Class: new-member
---


{{< member_header >}}
# Sonatus joins SOAFEE SIG

The SOAFEE community are excited to welcome Sonatus as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Sonatus

Sonatus helps automakers build software-defined vehicles that can evolve and adapt over their entire lifetimes through code-less updates that don’t require new software. Leveraging extensive software-defined and automotive expertise, the company compresses decades of digital innovation into scalable vehicle and cloud software solutions, empowering OEMs to innovate faster, reduce complexity and costs, and become more agile. Sonatus’ award-winning Digital Dynamics™  Vehicle Platform is currently in-market in Hyundai, Kia, and Genesis, and will be on the road in millions more vehicles by 2023. 

## Why Sonatus is joining the SOAFEE SIG

Sonatus brings a unique approach to software-defined vehicle development underpinned by its team's decades of experience across both IT and automotive: "Our mission at Sonatus is to empower automakers with complete control over the architecture needed to build and operate software-defined vehicles, which aligns closely with SOAFEE's objectives," said Jeffrey Chou, Co-founder and CEO of Sonatus. "We're proud to join SOAFEE and bring our expertise in software-defined technology to the community to help accelerate the development of next-generation vehicles and enable the broader digital transformation of the automotive industry."