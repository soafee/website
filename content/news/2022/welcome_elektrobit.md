---
Title: "Elektrobit joins SOAFEE SIG"
Description: "New member announcement – click for more details on Elektrobit"
Date: "2022-03-24"
Member: "voting/elektrobit"

Banner:
  Active: false
  Title: Elektrobit joins SOAFEE SIG
  Description: Elektrobit joins the SOAFEE SIG as a voting member.
  Background: "banner/banner3.jpg"

Card:
  Class: new-member
---


{{< member_header >}}
# Elektrobit joins SOAFEE SIG

The SOAFEE community are excited to welcome Elektrobit as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Elektrobit

Elektrobit is an award-winning and visionary global supplier of embedded and connected software products and services for the automotive industry. A leader in automotive software with over 30 years serving the industry, Elektrobit’s software powers over one billion devices in more than 100 million vehicles and offers flexible, innovative solutions for car infrastructure software, connectivity & security, automated driving and related tools, and user experience. Elektrobit is a wholly-owned, independently-operated subsidiary of Continental.

Elektrobit will bring in know-how and experience from ECU development, automotive standardization, functional safety, security and cloud native technologies. “The SOAFEE project aligns with Elektrobit’s commitment to helping our customers reduce complexity and maximize the reuse of software components as they build next-generation vehicles,” said Mike Robertson, vice president, global product management and strategy, Elektrobit. “We are proud to be a SOAFEE member. The cloud-native architecture will make it easier and more cost effective to develop software-defined vehicles and complements Elektrobit’s expertise and offerings in ECU development, automotive OS and functional safety.”
