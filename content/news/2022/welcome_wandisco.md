---
Title: WANdisco joins SOAFEE SIG
Description: New member announcement – click for more details on WANdisco
Date: 2022-09-13
Member: voting/wandisco

Banner:
  Active: false
  Title: WANdisco joins SOAFEE SIG
  Description: WANdisco joins the SOAFEE SIG as a voting member.
  Background: banner/banner2.jpg

Card:
  Class: new-member
---

{{< member_header >}}
# WANdisco joins SOAFEE SIG

The SOAFEE community are excited to welcome WANdisco as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About WANdisco

WANdisco is the first and only data activation platform for accelerating digital transformation at scale. WANdisco makes infinite data actionable across clouds and enterprises in real time. WANdisco customers unleash the business value of the cloud with zero downtime, data loss, or disruption to fuel AI and machine learning, create new services, and transform businesses.

## Why WANdisco is joining the SOAFEE SIG

“As software-defined vehicles increasingly dominate the automotive industry, the role of continuous data movement will become an integral component of connected car infrastructure,” said Dr. Frank Moser, Global Head of IoT Solutions and VP of Strategic Accounts at WANdisco. “In joining SOAFEE, WANdisco brings vast expertise in enterprise-scale data transfers between edge and cloud environments to key stakeholders across the industry, supporting the evolution of both hardware and software solutions to support next-generation automotive applications.”