---
Title: Apex.AI joins SOAFEE SIG
Description: New member announcement – click for more details on Apex.AI
Date: 2022-11-04
Member: voting/apexai

Banner:
  Active: false
  Title: Apex.AI joins SOAFEE SIG
  Description: Apex.AI joins the SOAFEE SIG as a voting member.
  Background: banner/banner2.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# Apex.AI joins SOAFEE SIG

The SOAFEE community are excited to welcome Apex.AI as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why Apex.AI are joining SOAFEE

The future of mobility is software-defined! SOAFEE aims to develop an open-standards-based cloud-native architecture for mixed-critical automotive design. It perfectly matches our strengths (functional safety, open-source, ROS, cloud-edge connectivity) and our strategic focus on software-defined vehicles.  

## About Apex.AI

Apex.AI provides safety-certified products enabling the transformation to the software-defined future of the automotive industry. Our products Apex.OS and Apex.Middleware are implementing cloud connectivity and the runtime environment to orchestrate automotive applications in mixed-critical systems. Apex.AI will bring its complete competence to take the next step toward software-defined vehicles.

