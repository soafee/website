---
Title: Vayavya Labs joins SOAFEE SIG
Description: New member announcement – click for more details on Vayavya Labs
Date: 2022-10-11
Member: voting/vayavya_labs

Banner:
  Active: false
  Title: Vayavya Labs joins SOAFEE SIG
  Description: Vayavya Labs joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---

{{< member_header >}}
# Vayavya Labs joins SOAFEE SIG

The SOAFEE community are excited to welcome Vayavya Labs as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why Vayavya Labs is joining the SOAFEE SIG

Automotive will be "Software on Wheels"!  We are witnessing a trend of "Shift-Left" strategy in the design & development of automotive systems & software. With SOAFEE as open standards, we strongly feel that we can contribute to further accelerating this "Left-Shift" trend, by using cloud-based architecture, microservices, containerisation and virtualization.

## About Vayavya Labs

Over the decade Vayavya Labs has provided Embedded Software Tools & Solutions to the Automotive & Semiconductor markets. We help the customers by enabling them to effectively adapt the Digital Twin strategy by providing virtual ECUs and safety-critical software. Besides Embedded software Vayavya Labs actively contributes to  ASAM's initiative for automation of verification & validation for Autonomous Vehicles & ADAS platforms using OPENSCENARIO2.0 standards.  More details on our tools, technology & offerings are available at: www.vayavyalabs.com