---
Title: FEV joins SOAFEE SIG
Description: New member announcement – click for more details on FEV
Date: 2022-11-08
Member: voting/fev

Banner:
  Active: false
  Title: FEV joins SOAFEE SIG
  Description: FEV joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# FEV joins SOAFEE SIG

The SOAFEE community are excited to welcome FEV as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why FEV is joining SOAFEE SIG

FEV Group GmbH joins SOAFEE SIG as a voting member to support the definition of the cloud-native development paradigm required for a new era of efficient edge workloads.

FEV contributes to the SOAFEE SIG With our experience from multiple projects in the automotive space, including an ongoing homegrown cross-functional application that addresses the SDV architecture as it relates to automotive and cloud-native development integration.

## About FEV

### FEV has always pushed the limits.

FEV is an internationally recognized leader of innovation across different sectors and industries. Professor Franz Pischinger laid the foundations by combining his background in academia and engineering with a great vision for continual progress. The company has supplied solutions and strategy consulting to the world's largest automotive OEMs and has supported customers through the entire transportation and mobility ecosystem.

### As the world continues to evolve, so does FEV.

That’s why FEV is unleashing its technological and strategic expertise into other areas. It applies its forward thinking to the energy sector. And its software and system know-how will enable the company to lead the way making intelligent solutions available to everyone. FEV brings together the brightest minds from different backgrounds and specialties to find new solutions for both current and future challenges.

### But FEV won’t stop there.

Looking ahead, FEV continues to push the limits of innovation. With its highly qualified 7,000 employees at more than 40 locations globally, FEV imagines solutions that don’t just meet today’s needs but tomorrow’s. Ultimately, FEV keeps evolving – to a better, cleaner future built on sustainable mobility, energy and software that drives everything. For the companies’ partners, its people and the world.

