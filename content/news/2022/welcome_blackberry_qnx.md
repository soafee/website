---
Title: BlackBerry QNX joins SOAFEE SIG
Description: New member announcement – click for more details on BlackBerry QNX
Date: 2022-09-22
Member: voting/blackberry_qnx

Banner:
  Active: false
  Title: BlackBerry QNX joins SOAFEE SIG
  Description: BlackBerry QNX joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# BlackBerry QNX joins SOAFEE SIG

The SOAFEE community are excited to welcome BlackBerry QNX as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why BlackBerry QNX are joining SOAFEE

As OEMs move to unified software architectures, collaborative initiatives like SOAFEE will be an important factor in the definition of next-generation software defined vehicles. BlackBerry QNX has a long history of embracing industry collaborations and we continue to work with our customers and partners to enable future automotive architectures.

## About BlackBerry QNX

BlackBerry® QNX® is a trusted supplier of safe and secure operating systems, hypervisors, frameworks and development tools, and provides expert support and services for building the world’s most critical embedded systems. The company’s technology is trusted in more than 215 million vehicles and is deployed in embedded systems around the world, across a range of industries including automotive, medical devices, industrial controls, transportation, heavy machinery and robotics. Founded in 1980, BlackBerry QNX is headquartered in Ottawa, Canada and was acquired by BlackBerry in 2010.
