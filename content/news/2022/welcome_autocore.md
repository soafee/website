---
Title: AutoCore joins SOAFEE SIG
Description: New member announcement – click for more details on AutoCore
Date: 2022-04-28
Member: voting/autocore

Banner:
  Active: false
  Title: AutoCore joins SOAFEE SIG
  Description: AutoCore joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# AutoCore joins SOAFEE SIG

The SOAFEE community are excited to welcome AutoCore as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About AutoCore

AutoCore is emerging as the world’s leading high-performance and safe intelligent mobility computing software and solution provider. The company focuses on Automotive Middleware, which is customizable for different EEA computing platforms of next-generation vehicles. AutoCore’s key products includes a multi-patented distributed operating system AutoCore.OS, a flexible AD system design AutoCore.SYS, and an autonomous driving development and testing and deployment tool chain AutoCore.Tools.

“Embedded systems are getting more and more powerful, and their capabilities can evolve and grow when they are integrated with the cloud. Our own experience in leading ARM-based Linux projects made us a big believer in open source software, and led us into co-founding the Autoware Foundation with TierIV to bring open source software to autonomous vehicles. In the Automotive sector, accountability and functional safety certification are very important, thus the foundational software layers are usually the domain of commercial software vendors. ARM, as the designer of the dominant architecture in the embedded edge, is taking the lead to build a framework that will allow open source and commercial software modules to be integrated, from the cloud to the edge. Autocore is happy to help deliver the Open AD Kit (for Autonomous Driving) as the first manifestation of the SOAFEE framework “ says Cheng Chen, CTO of Autocore.

