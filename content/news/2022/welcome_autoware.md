---
Title: Autoware joins SOAFEE SIG
Description: New member announcement – click for more details on Autoware
Date: 2022-11-15
Member: voting/autoware

Banner:
  Active: false
  Title: Autoware joins SOAFEE SIG
  Description: Autoware joins the SOAFEE SIG as a voting member.
  Background: banner/banner2.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# Autoware joins SOAFEE SIG

The SOAFEE community are excited to welcome Autoware as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why Autoware is joining SOAFEE

The Autoware Foundation is joining SOAFEE with the goal of enabling it’s open source software for autonomous driving as one of the first SOAFEE blueprints for the sofware-defined vehicle ecosystem.  The Autoware Open AD Kit enables developers to use Autoware in a micro-services architecture on the SOAFEE framework, along with cloud-native tools and CI/CD pipelines supporting shift left development and verification.

## About Autoware

Autoware is the world’s first “all-in-one” open-source software for self-driving vehicles hosted under the Autoware Foundation. Autoware is based on ROS 2, with contributions from the Autoware Foundation members and community at large following best-in-class software engineering practices. Further information at www.autoware.org.