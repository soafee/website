---
Title: HARMAN joins SOAFEE SIG
Description: New member announcement – click for more details on HARMAN
Date: 2022-07-26
Member: voting/harman

Banner:
  Active: false
  Title: HARMAN joins SOAFEE SIG
  Description: HARMAN joins the SOAFEE SIG as a voting member.
  Background: banner/banner4.jpg

Card:
  Class: new-member
---

{{< member_header >}}
# HARMAN joins SOAFEE SIG

The SOAFEE community are excited to welcome HARMAN as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About HARMAN

[HARMAN](https://www.harman.com) designs and engineers connected products and solutions for automakers, consumers, and enterprises worldwide, including connected car systems, audio and visual products, enterprise automation solutions; and services supporting the Internet of Things.

Our HARMAN Automotive Division is dedicated to the opportunities arising from the emergence of the Software Defined Vehicle (SDV). Focused on the in-cabin experience, we are delivering consumer experiences at automotive grade to help carmakers redefine the value proposition of the SDV with the consumer in mind.

## HARMAN’s contribution to the SOAFEE SIG

With the SOAFEE community, HARMAN joins other strong partners who share the same goal: to develop a seamless, cloud-based architecture that will accelerate the industry's evolution toward truly software-defined vehicles. Together we aim to define the framework for implementing feature on-demand capabilities and solutions for telematics data delivery and monetization. HARMAN will contribute with its deep knowledge of OTA update solutions and cloud services that already power billions of mobile devices and systems that are connected, integrated and secure across all platforms - from the workplace and home to the car and mobile devices.
