---
Title: Driveblocks joins SOAFEE SIG
Description: New member announcement – click for more details on Driveblocks
Date: 2022-05-19
Member: voting/driveblocks

Banner:
  Active: false
  Title: Driveblocks joins SOAFEE SIG
  Description: Driveblocks joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# Driveblocks joins SOAFEE SIG

The SOAFEE community are excited to welcome Driveblocks as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Driveblocks

driveblocks develops a modular and scalable software stack for autonomous driving. It’s focus area are heavy commercial vehicles: yard logistics, mining, agriculture and hub-to-hub transportation. The unique combination of open-source technologies, an open-architecture design and cutting edge algorithms enables the integration of the software modules on various customer hardware and software platforms. The heavily modular design allows the re-combination of modules to solve even the most challenging use-cases and the seamless integration with existing autonomy functionality while considering safety and certification.

The Munich based startup was founded by members of the Indy Autonomous Challenge winning team of the Technical University of Munich. The team has a strong track-record of solving real-world challenges in autonomous driving at the limits of technology by using a highly iterative and heavily simulation-driven development workflow and engaging with the open-source community.

## Why driveblocks is joining SOAFEE SIG

The interaction with an ecosystem of committed partners through open-source communities is one of the core values at driveblocks. We have chosen the SOAFEEE initiative as it provides a unique vision to streamline the development and deployment process, from feature development and evaluation in the cloud until the deployment of software modules on embedded computers within the vehicle. In addition, the work on standardization of mixed-criticality orchestration will be essential to increase modularity and move to a microservices oriented architecture for the autonomous driving software stack. These activities will help to speed-up the development of safe and efficient autonomy applications and enable a process of continuous improvement across various levels of the software stack.