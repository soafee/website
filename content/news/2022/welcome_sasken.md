---
Title: Sasken joins SOAFEE SIG
Description: New member announcement – click for more details on Sasken
Date: 2022-12-08
Member: voting/sasken

Banner:
  Active: true
  Title: Sasken joins SOAFEE SIG
  Description: Sasken joins the SOAFEE SIG as a voting member.
  Background: banner/banner2.jpg

Card:
  Class: new-member
---

{{< member_header >}}
# Sasken joins SOAFEE SIG

The SOAFEE community are excited to welcome Sasken as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why Sasken is joining the SOAFEE SIG

“Sasken has imagined, defined, developed, and delivered emerging automotive E/E technologies for over a decade now. The industry is converging on a ‘Software Define Architecture’ with SOAFEE leading the charge in standardizing modular vehicular architectures that enable cloud technologies to be combined with automotive functional safety and real-time requirements. This fits in perfectly with Sasken's full spectrum of chip-to-cognition offerings for automotive, spanning Smart Mobility, Integrated Cockpit, Connected Car, Autonomous Driving, Body Electronics, EV, Product Safety, and Security solutions. We have leveraged our expertise in hardware, connectivity, computing, the cloud, Android platforms, security, and standards to deliver tangible outcomes to both OEMs and Tier-1 suppliers across the product development life cycle.” said Venugopal Ramakrishnan, Chief Technology Officer at Sasken

## About Sasken

Sasken is a specialist in Product Engineering and Digital Transformation providing concept-to-market, chip-to-cognition R&D services to global leaders in Semiconductor, Automotive, Industrials, Consumer Electronics, Enterprise Devices, Satcom, Telecom, and Transportation industries. For over 30 years and with multiple patents, Sasken has transformed the businesses of 100+ Fortune 500 companies, powering more than a billion devices through its services and IP. For more information, please [visit](https://www.sasken.com/).


Established in 1989, Sasken employs around 1,400 people, operating from state-of-the-art centers in Bengaluru, Pune, Chennai, and Kolkata (India), Kaustinen and Tampere (Finland), and München (Germany). Sasken also has a presence across Japan and UK. Sasken has been listed in the National Stock Exchange and Bombay Stock Exchange Ltd. in Mumbai, India since its initial public offering in 2005.

Sasken’s solutions are backed by CMMI-DEV-V1.3-ML3, ISO 9001 (QMS) and ISO/IEC 27001 (ISMS) certifications. Sasken’s proprietary quality management system strengthens its business offerings and ensures client satisfaction. Sasken’s commitment to environment, health and safety is backed by its ISO 14001 (EMS) certification.

Sasken is compliant to ISO 26262 Road Vehicles-Functional Safety standard for Management of Functional Safety, Software Product Development, and Supporting Processes.