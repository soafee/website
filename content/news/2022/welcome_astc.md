---
Title: ASTC joins SOAFEE SIG
Description: New member announcement – click for more details on ASTC
Date: 2022-11-02
Member: voting/astc

Banner:
  Active: false
  Title: ASTC joins SOAFEE SIG
  Description: ASTC joins the SOAFEE SIG as a voting member.
  Background: banner/banner4.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# ASTC joins SOAFEE SIG

The SOAFEE community are excited to welcome ASTC as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why ASTC are joining SOAFEE

The explosion of software throughout the automotive industry demands scalable solutions for software development and test. ASTC's VLAB Works has been partnering with automotive companies across the supply chain to transition to this new reality, a reality that the SOAFEE initiative helps to extend beyond the vehicle and into the cloud. ASTC is joining the collaboration to help realise the potential we see in the SOAFEE initiative, to help extend software development and test beyond the hardware and into the cloud.

## About ASTC & VLAB Works

VLAB Works, an ASTC company, is an industry leader in software technology for the modelling, simulation, and virtual prototyping of embedded electronic systems. VLAB Works customers apply this technology to the virtualisation and automation of the embedded system development process. We help replace hardware with software, paper specifications with live simulations, late with early, slow with fast, limited with unlimited, complex with simple, and manual with automated.

VLAB Works technology and solutions enable production-level agile embedded development processes. These processes help accelerate product development schedules, develop optimised software and systems, improve quality, reduce risk, and eliminate the need for physical lab setups and hardware. VLAB Works helps customers design better products, do a better job in development, reduce the need for manufacturing and hardware, and help keep our planet greener.