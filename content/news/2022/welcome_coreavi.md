---
Title: CoreAVI joins SOAFEE SIG
Description: New member announcement – click for more details on CoreAVI
Date: 2022-08-01
Member: voting/coreavi

Banner:
  Active: false
  Title: CoreAVI joins SOAFEE SIG
  Description: CoreAVI joins the SOAFEE SIG as a voting member.
  Background: banner/banner1.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# CoreAVI joins SOAFEE SIG

The SOAFEE community are excited to welcome CoreAVI as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## Why CoreAVI are joining SOAFEE

CoreAVI architects safety critical graphics and compute solutions supporting safe autonomy, machine vision, AI and visualization applications in the automotive, unmanned vehicle and industrial IoT markets, as well as commercial and military avionics systems. CoreAVI's pedigree in the aerospace industry lends itself perfectly to the increased need for safety in today's software defined vehicles and autonomous systems. "CoreAVI is excited to join the ranks of influential automotive companies participating in SOAFEE," said Neil Stroud, VP Marketing and Business Development at CoreAVI. "We look forward to lending our vast experience in open standards as well as safety critical mixed criticality applications to SOAFEE and SOAFEE SIG and participating in defining the bright future of cloud-native architecture in tomorrow's software-defined vehicles." 

## About CoreAVI

CoreAVI is the global leader in architecting and delivering safety critical graphics and compute software drivers and libraries, embedded ‘system on chip’ and discrete graphics processor components, and certifiable platform hardware IP. CoreAVI’s comprehensive software suite enables development and deployment of complete safety critical solutions for automotive, industrial and aerospace applications requiring certification to the highest integrity levels coupled with full lifecycle support. CoreAVI’s solutions support both graphics and compute applications including safe autonomy, machine vision and AI in the automotive, unmanned vehicle and industrial IoT markets, as well as commercial and military avionics systems. www.coreavi.com