---
Title: Linaro joins SOAFEE SIG
Description: New member announcement – click for more details on Linaro
Date: 2022-03-31
Member: voting/linaro

Banner:
  Active: false
  Title: Linaro joins SOAFEE SIG
  Description: Linaro joins the SOAFEE SIG as a voting member.
  Background: banner/banner3.jpg

Card:
  Class: new-member
---


{{< member_header >}}
# Linaro joins SOAFEE SIG

The SOAFEE community are excited to welcome Linaro as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About Linaro

Linaro works with businesses and open-source communities to develop software on Arm-based technology. Through its collaborative projects, Linaro strives to solve Arm ecosystem quality issues, deliver upstream support for Architecture and OS requirements, and solve problems of fragmentation which limit market deployment. Linaro intends to contribute firmware, cybersecurity (firmware, OS, hypervisor), heterogeneous (cortex-A + Cortex-R/M) platform features and time sensitive applications (Real time in OS/hypervisor, TSN) to the SOAFEE project.

“Over the past three years, Linaro has made significant contributions to projects such as OP-TEE, U-Boot and the Linux kernel through its work on integrating time sensitive networking and standardized security with OTA”, said Francois Ozog, Director of Business Development
at Linaro. “We are excited to bring this expertise to the SOAFEE project which we believe will accelerate the pace at which software-defined vehicles are realised.”