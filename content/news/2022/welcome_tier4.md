---
Title: "TIER IV joins SOAFEE SIG"
Description: "New member announcement – click for more details on TIER IV"
Date: "2022-03-15"
Member: "voting/tieriv"

Banner:
  Active: false
  Title: TIER IV joins SOAFEE SIG
  Description: TIER IV joins the SOAFEE SIG as a voting member.
  Background: "banner/banner5.jpg"

Card:
  Class: new-member
---

{{< member_header >}}
# TIER IV joins SOAFEE SIG

The SOAFEE community are excited to welcome TIER IV as a voting member of the SOAFEE SIG.
{{</ member_header >}}

## About TIER IV

TIER IV is the creator of [Autoware](https://www.autoware.org/), the world’s first open-source software for autonomous driving, and provides Autoware-based full-stack solutions for the commercialization of autonomous vehicles.

“TIER IV values open-source software and is working with the Autoware Foundation members to develop the Open AD kit based on SOAFEE  as a reference framework to accelerate the AD software development for software-defined vehicles. We look forward to working together with SOAFEE members to enable cloud-native software development for autonomous vehicles."  -  Shinpei Kato, Founder & CTO, TIER IV, Co-Founder & Chairman, The Autoware Foundation.