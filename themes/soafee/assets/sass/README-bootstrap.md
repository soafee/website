# Use of Bootstrap

The SOAFEE theme makes use of [Bootstrap](https://getbootstrap.com/), but we
need to modify some of the styling with a SCSS pre-processor.

To achieve this, we have taken a vanilla copy of the scss and dist directories
from an official upstream release of bootstrap.  None of the bootstrap files
have been modified in order to comply with the MIT license of Bootstrap.

The current release being used is [5.1.3](https://github.com/twbs/bootstrap/archive/v5.1.3.zip).