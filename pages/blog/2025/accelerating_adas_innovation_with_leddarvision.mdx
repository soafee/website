---
title: Accelerating ADAS Innovation with LeddarVision Using SOAFEE
description: Accelerating ADAS Innovation with LeddarVision Using SOAFEE
date: 24 February 2025
body: By Raj Reddy, Director of Product Management, LeddarTech
---

import BlogContainer from '@/components/layouts/blog-container';
import DecoratedText from '@/components/decorated_text';
import logo from 'public/logos/leddartech.png';
import image_1 from 'public/images/leddartech_1.png';
import image_2 from 'public/images/leddartech_2.png';

<main className="text-soafee-black">
  <section className="bg-soafee-gray">
    <DecoratedText className="wrapper-lg pb-8 pt-[120px] md:pb-20 md:pt-[152px] md:h-min-[400px] flex flex-col" decoration="right" decorationClass="fill-soafee-light-gray">
      <div className="max-w-[820px] mb-4">
        ## Accelerating ADAS Innovation with LeddarVision Using SOAFEE
      </div>
      <div className="max-w-[490px]">
        <div className="typography-mobile-b1 lg:typography-desktop-b3 mb-4">
          By Raj Reddy, Director of Product Management, LeddarTech
        </div>
      </div>
    </DecoratedText>
  </section>

  <section className='px-4 pt-4 lg:pt-16 lg:px-16 w-full flex justify-center items-center flex-col bg-soafee-light-gray'>
    <img src={logo.src} className='max-h-44' />
    <iframe
        type="text/html"
        id="ytplayer"
        className="aspect-video w-full max-w-6xl"
        src="https://www.youtube.com/embed/9rvwiUoEZvk"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
      <p className='text-center typography-mobile-b2 lg:typography-desktop-b3'>LeddarVision: LeddarTech’s perception software based on low-level fusion</p>
  </section>
 

  <BlogContainer>
    <BlogContainer.title>
      ## A New Era of Intelligent Vehicle Perception
      <img src={image_1.src} className='mt-4 w-full rounded-3xl' />
    </BlogContainer.title>
    <BlogContainer.body>
      <div className='mt-10 lg:mt-28'>
        ADAS and autonomous driving require more than fragmented perception models-they demand a
        fully integrated, cloud-powered future. At LeddarTech, we are pioneering AI-driven, multimodal
        sensor fusion, setting new benchmarks for precision, efficiency and scalability in vehicle
        perception.
        <br />
        <a className='text-blue-600 !!underline' href='https://leddartech.com/solutions/leddarvision/'>LeddarVision™</a>
        combines data from multiple sensor types-including cameras, 
        radar and LiDAR-to create a precise real-time environmental model. This approach enhances accuracy and
        adaptability, making advanced ADAS and autonomous driving solutions more robust and scalable.
      </div>
    </BlogContainer.body>
  </BlogContainer>

  <BlogContainer>
    <BlogContainer.title>
      ## Revolutionizing Perception with LeddarVision
    </BlogContainer.title>
    <BlogContainer.body>
      Traditional object-level fusion processes sensor data separately, leading to incomplete or delayed object detection and classification. LeddarVision disrupts this model by integrating raw sensor data at a foundational level, enabling more precise environmental perception and a seamless 360° understanding of the vehicle’s surroundings.
      <br />
      Through low-level sensor fusion, LeddarVision enhances:
      <br />
      <ul>
        <li><strong>Object Detection and Classification</strong> - Identifies vehicles, pedestrians and obstacles with greater accuracy.</li>
        <li><strong>Reaction Times</strong> - Enables faster ADAS decision making.</li>
        <li><strong>Performance in Challenging Conditions</strong> - Maintains reliability in fog, rain and low-light environments.</li>
      </ul>
    </BlogContainer.body>
  </BlogContainer>

  <BlogContainer>
    <BlogContainer.title>
      ## Advancing ADAS with Arm and SOAFEE
      <img src={image_2.src} className='mt-4 w-full rounded-3xl' />
    </BlogContainer.title>
    <BlogContainer.body>
      <div className='mt-10 lg:mt-28'>
        The automotive industry is shifting toward cloud-native intelligence, enabling faster software validation, real-time deployment and seamless over-the-air updates. Through our collaboration with Arm and SOAFEE, LeddarTech is driving a scalable, cloud-to-edge pipeline that accelerates ADAS innovation and deployment.
        <br />
        SOAFEE provides a unified, hardware-agnostic framework, allowing automakers, Tier 1 suppliers, and AI developers to integrate advanced perception software more efficiently. 
        
        SOAFEE bridges high-performance computing (HPC) in vehicles with cloud infrastructures, providing a flexible and modular development environment. 
      </div>
    </BlogContainer.body>
  </BlogContainer>

  <BlogContainer>
    <BlogContainer.title>
      ## Key Benefits of SOAFEE for ADAS Development
    </BlogContainer.title>
    <BlogContainer.body>
      <ul>
        <li>
          <strong>Efficiency and Scalability</strong>: Reduces development time and complexity while enabling seamless deployment across multiple vehicle platforms and architectures.
        </li>
        <br />
        <li>
          <strong>Cloud-Native Validation and Edge Execution</strong>: Ensures reliability from simulation to real-world operation, accelerating validation cycles and shortening time-to-market.
        </li>
        <br />
        <li>
          <strong>Standardization and Integration</strong>: Unifies ADAS perception, middleware, and AI applications, eliminating fragmented system architectures for streamlined development.
        </li>
        <br />
        <li>
          <strong>Safety, Security, and Compliance</strong>: Supports automotive-grade safety standards, ensuring reliable and secure mass-market deployment.
        </li>
      </ul>
    </BlogContainer.body>
  </BlogContainer>

  <BlogContainer>
    <BlogContainer.title>
      ## LeddarVision on SOAFEE: Seamless Transition from Cloud to Edge
    </BlogContainer.title>
    <BlogContainer.body>
      We’ve successfully demonstrated LeddarVision on an Arm-based virtual server in the cloud using AWS, Graviton2 CPUs, optimizing cloud-native perception processing. After testing in the cloud, deploying the same LeddarVision application to an automotive processor at the edge is made seamless with SOAFEE, which leverages containerization for easy transition between environments.
      <br />
      <ul>
        <li>
          <strong>Lighter Configurations (1V2R):</strong> Delivers up to 4 x real-time processing speeds, ensuring low-latency, high-efficiency perception.
        </li>
        <li>
          <strong>Heavier Configurations (5V5R):</strong> Maintains near real-time performance, proving scalability for high-compute ADAS applications.
        </li>
      </ul>
      This cloud-to-edge workflow enables automakers and Tier 1 suppliers to validate high-fidelity ADAS models early in development, cutting costs and speeding up the overall development process.
    </BlogContainer.body>
  </BlogContainer>

  <BlogContainer>
    <BlogContainer.title>
      ## Real-Time Visualization and Perception Processing
    </BlogContainer.title>
    <BlogContainer.body>
      LeddarVision integrates seamlessly into SOAFEE’s containerized architecture, running in the cloud while delivering real-time visualization via a local GUI.
      <br />
      Key Visualization Features:
      <br />
      <ul>
        <li>
          <strong>Camera Stream Panel</strong> - Displays lane detections and object tracking in real time.
          <ul style={{listStyleType: "circle"}}>
            <li>
              Green = Ego lane | Blue = Left lane | Red = Right lane
            </li>
            <li>
              Red bounding boxes highlight closest in-path vehicles (CIPV).
            </li>
          </ul>
        </li>
        <li>
          <strong>3D View Panel</strong> - Tracks and visualizes detected objects and road elements in real time.
        </li>
        <li>
          <strong>Bird’s Eye View (BEV) Panel</strong> - Provides a 200 m top-down perspective, offering a complete situational awareness model.
        </li>
      </ul>
      By processing multi-sensor data simultaneously, LeddarVision improves vehicle awareness, enabling safer navigation and more reliable ADAS decision making.
    </BlogContainer.body>
  </BlogContainer>
  
  <BlogContainer>
    <BlogContainer.title>
      ## Defining the Future of Intelligent Mobility
      <br />
      <p><strong>Explore the future of ADAS with LeddarTech at <a className='text-blue-600 !underline' href='https://leddartech.com'>leddartech.com</a> </strong></p>
    </BlogContainer.title>
    <BlogContainer.body>
      LeddarVision is more than an ADAS solution—it’s the foundation for the next generation of safe and intelligent mobility. Our cloud-native, AI-driven perception software, in collaboration with Arm and SOAFEE, is revolutionizing how vehicles see, interpret and react in real-world environments.
      <br />
      As the industry moves toward software-defined vehicles, LeddarTech is leading the way—driving safety, efficiency and scalability in automotive perception.
    </BlogContainer.body>
  </BlogContainer> 
</main>