export interface Seminarlink {
  title: string;
  link: string;
}

export const seminarMaterialsData: Seminarlink[] = [
  {
    title: 'Arm: “SOAFEE SIG update and future plans”',
    link: '/files/soafee-seminar-open-and-arm-presentation.pdf'
  },
  {
    title: 'AWS: “The Rise of Software-Defined Vehicles and Cloud-to-Vehicle Edge Parity”',
    link: '/files/soafee-seminar-aws.pdf'
  },
  {
    title:
      'Black Sesame Technologies: “The application of micro-kernel based operating system in cross-domain computing for intelligent vehicles”',
    link: '/files/soafee-seminar-black-sesame.pdf'
  },
  {
    title: 'DENSO: “System design and verification for Mixed-Criticality Systems in SDV”',
    link: '/files/soafee-seminar-denso.pdf'
  },
  {
    title: 'LG Electronics: “Collaboration with SOAFEE for Vehicle Service Orchestration and Introduction of PICCOLO”',
    link: '/files/soafee-seminar-lge.pdf'
  },
  {
    title: 'Linaro: “Accelerating SDV hardware and software interoperability with the SOAFEE integration lab”',
    link: '/files/soafee-seminar-linaro-integration-lab.pdf'
  },
  {
    title:
      'Panasonic Automotive “Systems: Enabling a Software-Defined Automotive Edge with VirtIO based Device Virtualization”',
    link: '/files/soafee-seminar-panasonicautomotive.pdf'
  },
  {
    title: 'Renesas “Electronics: Proposal of BSP driver standardization with System Ready”',
    link: '/files/soafee-seminar-renesas.pdf'
  },
  {
    title: 'ThunderSoft: “Cloud Native in Vehicle OS and AD+Cockpit Fusion Architecture”',
    link: '/files/soafee-seminar-thundersoft.pdf'
  }
];
