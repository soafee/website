export interface Seminarlink {
  title: string;
  link: string;
}

export const seminarMaterialsData: Seminarlink[] = [
  {
    title: 'LG: “Leading the Future of AI-Powered Software-Defined Vehicles Together”',
    link: '/files/korea_seminar/02_SOAFEE_Korea_Welcome_remark_final.pdf'
  },
  {
    title: 'SDV Alliance: “SOAFEE Update”',
    link: '/files/korea_seminar/03_SOAFEE_Korea_SIG_SDV_Alliance_Update_and_Intro_v3.pdf'
  },
  {
    title: 'LG: “Beyond the Road: Navigating the SDV Transition”',
    link: '/files/korea_seminar/04_SOAFEE_Beyond_the_Road_Navigating_the_SDV_Transition_Valentin_LGElectronics.pdf'
  },
  {
    title: 'KATECH: “Electronic Architecture Design to Realise SDV”',
    link: '/files/korea_seminar/05_240924_Electronic_Architecture_Design_to_realize_SDV_revised.pdf'
  },
  {
    title: 'Kookmin University: “Impact of SDV Advancements on the Automotive Industry Ecosystem and Implications”',
    link: '/files/korea_seminar/06_SOAFEE_KOREA_Impact_of_SDV_GM_Jung_KookminUniv.pdf'
  },
  {
    title: 'Hyundai MOBIS: “Open Source and Standardisation”',
    link: '/files/korea_seminar/07_Open_source_and_Standardization.pdf'
  },
  {
    title: 'Siemens: “Automotive system development using accelerated pre-silicon simulation environment”',
    link: '/files/korea_seminar/08_SOAFEE_seminar_20240924_share.pdf'
  },
  {
    title: 'Denso: “SDV Challenges and Cloud-Native System Design Approach”',
    link: '/files/korea_seminar/09_2024-09-24_System_Design_Denso_APAC24_r4.pdf'
  },
  {
    title: 'The Autoware Foundation Update',
    link: '/files/korea_seminar/10_Autoware_Foundation_SOAFEE_Workshop_Presentation_v1.0.pdf'
  },
  {
    title: 'AutoCore: “When SDV meets reality”',
    link: '/files/korea_seminar/11_When_SDV_meets_reality_SOAFEE_Korea.pdf'
  }
];
