import glob from 'fast-glob';
import { promises as fs } from 'fs';
import matter from 'gray-matter';

type PageWithFrontmatter = {
  frontmatter: any;
  slug: string;
};

const filterByType = (page: PageWithFrontmatter, type: string) => {
  if (Array.isArray(page.frontmatter.type)) return page.frontmatter.type.findIndex((e: string) => e === type) != -1;
  else return page.frontmatter.type === type;
};

const findPages = async (path: string, type?: string) => {
  let paths = glob.sync('pages/' + path + '/**/*.mdx');

  let pages = await Promise.all(
    paths.map(async (filename) => {
      //const filePath = path.join(postsDirectory, filename)
      const source = await fs.readFile(filename, 'utf8');
      const { content, data: frontmatter } = matter(source);
      let slug = filename
        .replace(/^pages\//, '') // Remove leading 'pages'
        .replace(/\.mdx$/, '') // Remove '.mdx'
        .replace(/\/index/, ''); // Remove '/index' if present
      return {
        frontmatter: frontmatter,
        slug: slug
      };
    })
  );

  if (type) {
    return pages.filter((page) => filterByType(page, type));
  } else {
    return pages;
  }
};

export { findPages };
