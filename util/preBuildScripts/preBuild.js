import generateMembers from './generateMembers.js';
import generateEvents from './generateEvents.js';
import generateBlogPosts from './generateBlogPosts.js';
import generateNews from './generateNews.js';
import checkMemberLogos from './checkMemberLogos.js';

let outputDir = './autogen';

checkMemberLogos();
generateMembers(outputDir);
generateEvents(outputDir);
generateBlogPosts(outputDir);
generateNews(outputDir);
